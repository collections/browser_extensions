# Changelog

## 2022-05-30

### Hinzugefügt

- Informationen über die native HTTPS-everywhere Funktion in modernen Browsern

### Korrigiert

- Firefox Addon Icon URLS

### Aktualisiert

- HTTPS everywhere Position an das Ende verschoben

## 2021-09-12

### Hinzugefügt

  - initiale Liste von Erweiterungen
  - Mute Incognito Tab Erweiterung
  - NoScript Erweiterung
  - `source`-Kennzeichnung
  - Links zur jew. Seite der Edge-Erweiterung
  - Erweiterungen: Chameleon, CanvasBlocker, Canvas Defender and Cookie AutoDelete, FirefoxPWA und Brave search
  - Icons aller Erweiterungen
  - Quell-Tag
  - verfügbare Open-Source-Lizenzen
  
### Korrigiert
  
  - Einführung
  - Codeformatierung
  - Trennzeichen `\|` zwischen `[Firefox]` und `[chocolatey]` eingefügt
  - Vanilla Cookie Manager Icon verkleinert
  
### Übersetzt
  
  - README.md
  - CHANGELOG.md
  - KeePassXC-Browser Beschreibung
  
### Aktualisiert

  - Canvas Defender Beschreibung gekürzt
