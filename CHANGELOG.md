# Changelog

## 2022-05-30

### Added

- info about native browser ability to use HTTPS everywhere

### Corrected

- new Firefox addon icon URLS

### Updated

- HTTPS everywhere position to bottom

## 2021-09-12

### Added

- initial list of extensions
- Mute Incognito Tab extension
- NoScript extension
- `source` tag
- edge links for all extensions that support edge
- Chameleon, CanvasBlocker, Canvas Defender and Cookie AutoDelete, FirefoxPWA and Brave search extensions
- extension icons
- source tag
- available open source licenses

### Corrected

- introduction
- code format
- separator char `\|` added between `[Firefox]` and `[chocolatey]`
- resized Vanilla Cookie Manager icon

### Updated

- shortened Canvas Defender description
